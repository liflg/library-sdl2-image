Website
=======
https://www.libsdl.org/projects/SDL_image/

License
=======
zlib license (see the file source/COPYING.txt)

Version
=======
2.0.4

Source
======
SDL2_image-2.0.4.tar.gz (sha256: e74ec49c2402eb242fbfa16f2f43a19582a74c2eabfbfb873f00d4250038ceac)

Requires
========
* png16
* SDL2
